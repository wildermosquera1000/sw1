﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(proyectowilder.Startup))]
namespace proyectowilder
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
