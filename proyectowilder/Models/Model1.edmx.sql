
-- --------------------------------------------------
-- Entity Designer DDL Script for SQL Server 2005, 2008, 2012 and Azure
-- --------------------------------------------------
-- Date Created: 07/15/2015 08:31:07
-- Generated from EDMX file: c:\users\wilder alberto\documents\visual studio 2013\Projects\proyectowilder\proyectowilder\Models\Model1.edmx
-- --------------------------------------------------

SET QUOTED_IDENTIFIER OFF;
GO
USE [wilderam];
GO
IF SCHEMA_ID(N'dbo') IS NULL EXECUTE(N'CREATE SCHEMA [dbo]');
GO

-- --------------------------------------------------
-- Dropping existing FOREIGN KEY constraints
-- --------------------------------------------------


-- --------------------------------------------------
-- Dropping existing tables
-- --------------------------------------------------


-- --------------------------------------------------
-- Creating all tables
-- --------------------------------------------------

-- Creating table 'usuarioSet'
CREATE TABLE [dbo].[usuarioSet] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [pasword] nvarchar(max)  NOT NULL
);
GO

-- Creating table 'personaSet'
CREATE TABLE [dbo].[personaSet] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [nombre] nvarchar(max)  NOT NULL,
    [apellido] nvarchar(max)  NOT NULL,
    [correo] nvarchar(max)  NOT NULL,
    [telefono] nvarchar(max)  NOT NULL
);
GO

-- --------------------------------------------------
-- Creating all PRIMARY KEY constraints
-- --------------------------------------------------

-- Creating primary key on [Id] in table 'usuarioSet'
ALTER TABLE [dbo].[usuarioSet]
ADD CONSTRAINT [PK_usuarioSet]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'personaSet'
ALTER TABLE [dbo].[personaSet]
ADD CONSTRAINT [PK_personaSet]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- --------------------------------------------------
-- Creating all FOREIGN KEY constraints
-- --------------------------------------------------

-- --------------------------------------------------
-- Script has ended
-- --------------------------------------------------